import operator
""" Network Failure Point Problem
https://docs.google.com/document/d/1md0U22KmmfT1HA8VzjYU4uqPy76cXgGBnJxLB1PJDuU/edit#
"""

def identify_router(graph):
	"""
	Given a directed graph, identify the node with the most number of connections. The nodes represent routers labeled from 1 to 6
	The graph input will be represented as a dictionary, with the keys representing nodes and the values 
	are a list of all nodes that the key has a directed edge towards.
	Return the label of the node with the most connections

	Runtime Analysis:
	Essentially, this problem can be abstracted to find the "node with the most number of inbound plus outbound edges".
	Worst Case runtime of identify_router is O(n^2). I could not find a more optimal solution, but after thinking about this, I don't think 
	one exists.
	In the code below, getting the outbound links is trivial, it is done in linear time, O(n). 
	Getting the inbound links requires us to traverse each list in the adjacency list provided as the parameter. Since
	the total number of possbile edges could be n * (n - 1) (which is in O(n^2)) where n represents the number of nodes,
	that means to get the node with the max links requires us to traverse over each edge, hence proving that a runtime of O(n^2) 
	is the fastest worst case runtime of this algorithm.
	"""

	num_links = {}

	# The number of outbound links of a node is the length of the node's value list.
	for node in graph.keys():
		num_links[node] = len(graph[node])
	
	
	# The number of inbound links of a node are the number of times it appears in other nodes' list.
	for node in num_links.keys():	
		for directed_node in graph[node]:
			num_links[directed_node] += 1

	# Return the node with the highest sum of the inbound and outbound links
	return max(num_links.items(), key=operator.itemgetter(1))[0]

if __name__ == '__main__':
	
	adj_list = {1: [2, 3, 4, 5, 6], 2: [1, 3, 4, 5, 6], 3: [1, 2, 4, 5, 6], 4: [1, 2, 3, 5, 6], 
                    5: [1, 2, 3, 4, 6],
                    6: [1, 2, 3, 4, 5]}
	print(identify_router(adj_list))