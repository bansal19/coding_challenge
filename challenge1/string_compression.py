
def compress_string(compressStr):
    """ Given a string (str), returns a compressed string. 
    A compressed string is one that follows a letter with the number of times that 
    it is present. If the length of the string is not reduced, return the original string.

    >>> compress_string('aaaabbbccddddddee')
    a4b3c2d6e2

    Runtime of function: 
    Worst Case runtime is n + 3 which is in O(n) where n is the input size. We iterate over the 
    length of the string once, item by item.
    """
    # Initialise the resulting string as empty, set the letterCount to 1.
    resultString = ""
    letterCount = 1

    for i in range(len(compressStr) - 1):
        if compressStr[i] == compressStr[i + 1]:
            # The next character is the same as the previous character so we can increase the letterCount
            letterCount += 1
        else:
            # The next character is different, so append character and letterCount to resultString and 
            # initialise letterCount to 1.
            resultString += compressStr[i] + str(letterCount)
            letterCount = 1
    
    # The last (repeating or otherwise) character is not accounted for, so append it the resultString.
    resultString += compressStr[len(compressStr) - 1] + str(letterCount)
    
    # If the resultString is exactly twice the size of the compressStr, then there were no 
    # consecutive repeating characters and we can just return the original string.
    if len(resultString) == 2 * len(compressStr):
        return compressStr

    return resultString