import unittest
from string_compression import compress_string
from identify_router import identify_router

class TestStringCompression(unittest.TestCase):
    
    # Testing Question 1, string compression:
    def test_long_string(self):
        self.assertEqual(compress_string('aaaabbbccddddddee'), 'a4b3c2d6e2')

    def test_no_compression(self):
        self.assertEqual(compress_string('abcdefg'), 'abcdefg')

    def test_single_letter(self):
        self.assertEqual(compress_string('a'), 'a')

    def test_repeating_letters(self):
        self.assertEqual(compress_string('aaabbbcccaaa'), 'a3b3c3a3')


    # Testing Question 2, identify router:
    def test_one(self):
        adj_list = {1: [2, 3, 4], 2: [1, 3, 4], 3: [], 4: [5], 5:[1, 6], 6: []}
        self.assertEqual(identify_router(adj_list), 1)
    
    def test_complete_graph(self):
        adj_list = {1: [2, 3, 4, 5, 6], 2: [1, 3, 4, 5, 6], 3: [1, 2, 4, 5, 6], 4: [1, 2, 3, 5, 6], 
                    5: [1, 2, 3, 4, 6],
                    6: [1, 2, 3, 4, 5]}
        
        self.assertEqual(identify_router(adj_list), 1)

    def test_small_graph(self):
        adj_list = {1: [], 2: [], 3: [1, 3], 4: [5], 5: [], 6: [3]}
        self.assertEqual(identify_router(adj_list), 3)

if __name__ == '__main__':
    unittest.main()