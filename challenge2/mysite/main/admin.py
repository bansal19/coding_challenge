from django.contrib import admin
from .models import Subscription
# Register your models here.

class SubscriptionAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Title/date", {"fields": ["name", "description"]}),
        ("Price", {"fields": ["price"]})
    ]
    
admin.site.register(Subscription)