from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
import stripe
# Create your models here.

class Subscription(models.Model):
    
    name = models.CharField(max_length=200)
    description = models.TextField()
    price = models.FloatField(default=49.99)
    subscribed = models.BooleanField(default=False)
    user_id = User.email

    def __str__(self):
        return self.name