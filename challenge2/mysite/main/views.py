from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Subscription
from .forms import RegisterForm
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from django.conf import settings
from django.contrib.auth.models import User
import stripe

# Create your views here.

stripe.api_key = settings.STRIPE_SECRET_KEY

def homepage(request):

    return render(request=request,
                  template_name="homepage/homepage.html",
                  context={"tutorials": Subscription.objects.all})

def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"New Account Created: {username}")
            login(request, user)
            return redirect("/")
        else:
            for msg in form.error_messages:
                messages.error(request, form.error_messages[msg])
    else:
        form = RegisterForm()

    return render(request, "register/register.html", {"form":form})

def logout_request(request):
    
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("main:homepage")


def subscription(request):
    
    # Set your secret key. Remember to switch to your live secret key in production!
    # See your keys here: https://dashboard.stripe.com/account/apikeys
    if request.method == 'GET':
        session = stripe.checkout.Session.create(
        payment_method_types=['card'],
        subscription_data={
            'items': [{
            'plan': 'plan_HBAOw5mziphTLO',
            }],
        },
        success_url='http://127.0.0.1:8000/success?session_id={CHECKOUT_SESSION_ID}',
        cancel_url='http://127.0.0.1:8000/',
        )

        context = { 'session_id': session.id }

        return render(request, "subscription/subscription.html", context)
    elif request.method == 'POST':
        user_subscription = Subscription.objects.get(User.email)

        token = request.POST.get('stripeToken')
        stripe.Charge.create(
            amount = 4999,
            currency ='usd',
            source=token,
            description="Charge for WhatsBusy premium service"
        )
        
        user_subscription.subscribed = True